'use strict'

var mongoose = require('mongoose')
var Schema = mongoose.Schema

var CriptomonedasSchema = Schema({
  id: String,
  criptomoneda: String
})

module.exports = mongoose.model('criptomonedas', CriptomonedasSchema)  //Checar este


/*"nombre": "luis",
    "apellidop": "falcon",
    "apellidom": "sanchez",
    "password": "1234",
    "Movimientos": [{
        "comercio": "Rappi",
        "cantidad": {
            "$numberInt": "550"
        }
    }, {
        "comercio": "Uber",
        "cantidad": {
            "$numberInt": "111"
        }
    }, {
        "comercio": "Jumex",
        "cantidad": {
            "$numberInt": "213"
        }
    }]*/